package managers

import (
	"encoding/json"
	"fmt"
	"os"
	"strings"

	"gitlab.com/staticvoidlabs/gotwibo/models"
)

var mCurrentConfig models.Config
var mAuthObject models.AuthObject
var mTwitterIDs []string

// Public functions.
func GetCurrentConfig() models.Config {

	return processConfigFile()
}

func GenerateAuthObject() {

	mAuthObject = processAuthFile()
}

// Private functions.
func processConfigFile() models.Config {

	var currentConfig models.Config

	configFile, err := os.Open("./config.json")
	defer configFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&currentConfig)

	mCurrentConfig = currentConfig

	processTwitterIDs()

	return currentConfig
}

func processAuthFile() models.AuthObject {

	var tmpAuthObject models.AuthObject

	authFile, err := os.Open("./auth.json")
	defer authFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(authFile)
	jsonParser.Decode(&tmpAuthObject)

	mAuthObject = tmpAuthObject

	return tmpAuthObject
}

func processTwitterIDs() {

	tmpTwitterIDListClean := strings.Split(mCurrentConfig.TwitterIDs, ",")

	mTwitterIDs = tmpTwitterIDListClean
}
