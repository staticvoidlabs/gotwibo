package managers

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"strconv"

	"github.com/g8rswimmer/go-twitter"
	"gitlab.com/staticvoidlabs/gotwibo/models"
)

var mTweetsRepo models.TweetsRepo
var mMediaObjectsRepo models.MediaObjectsRepo

type authorize struct {
	Token string
}

func (a authorize) Add(req *http.Request) {
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %s", a.Token))
}

func InitRepos() {

	token := flag.String("token", mAuthObject.BearerToken, "twitter API token")
	id := flag.String("id", "29276141", "user id")
	flag.Parse()

	user := &twitter.User{
		Authorizer: authorize{
			Token: *token,
		},
		Client: http.DefaultClient,
		Host:   "https://api.twitter.com",
	}
	tweetOpts := twitter.UserTimelineOpts{
		TweetFields: []twitter.TweetField{
			twitter.TweetFieldAttachments,
			twitter.TweetFieldAuthorID,
			twitter.TweetFieldContextAnnotations,
			twitter.TweetFieldConversationID,
			twitter.TweetFieldCreatedAt,
			twitter.TweetFieldEntities,
			twitter.TweetFieldID,
			twitter.TweetFieldInReplyToUserID,
			twitter.TweetFieldReferencedTweets,
			twitter.TweetFieldSource,
			twitter.TweetFieldText,
		},
		MediaFields: []twitter.MediaField{
			twitter.MediaFieldPreviewImageURL,
			twitter.MediaFieldURL,
			twitter.MediaFieldMediaKey,
		},
		Expansions: []twitter.Expansion{
			twitter.ExpansionAuthorID,
			twitter.ExpansionReferencedTweetsID,
			twitter.ExpansionReferencedTweetsIDAuthorID,
			twitter.ExpansionEntitiesMentionsUserName,
			twitter.ExpansionAttachmentsMediaKeys,
			twitter.ExpansionInReplyToUserID,
			//twitter.ExpansionAttachmentsMediaKeys,
		},
		MaxResults: 15,
	}

	userTweets, err := user.Tweets(context.Background(), *id, tweetOpts)
	//var tweetErr *twitter.TweetErrorResponse

	if err != nil {
		fmt.Println(err)

		return
	}

	for _, tweet := range userTweets.Tweets {

		var tmpTweet models.Tweet

		tmpTweet.TweetID = tweet.ID
		tmpTweet.Text = tweet.Text
		tmpTweet.Type = getTweetType(tweet)
		tmpTweet.CreatedAt = tweet.CreatedAt

		for _, mediaObject := range userTweets.Includes.Medias {

			if tweet.Attachments.MediaKeys != nil && tweet.Attachments.MediaKeys[0] == mediaObject.Key {

				var tmpMediaObject models.MediaObject

				tmpMediaObject.MediaKey = mediaObject.Key
				tmpMediaObject.URL = mediaObject.URL
				tmpMediaObject.Type = mediaObject.Type

				tmpTweet.MediaObjects = append(tmpTweet.MediaObjects, tmpMediaObject)

			}

		}

		mTweetsRepo.Tweets = append(mTweetsRepo.Tweets, tmpTweet)

	}

	printTweetsRepo()

}

func getTweetType(tweet twitter.TweetObj) string {

	var tmpReturnString = ""

	if tweet.Text[0:2] == "RT" {
		tmpReturnString = "RETWEET"
	} else if tweet.InReplyToUserID == "" {
		tmpReturnString = "TWEET"
	} else if tweet.InReplyToUserID == tweet.AuthorID {
		tmpReturnString = "FOLLOWUP"
	} else if tweet.InReplyToUserID != tweet.AuthorID {
		tmpReturnString = "ANSWER"
	} else {
		tmpReturnString = "OTHER"
	}

	return tmpReturnString
}

func printTweetsRepo() {

	for i, e := range mTweetsRepo.Tweets {

		tmpIndex := strconv.Itoa(i)
		tmpType := e.Type
		tmpText := e.Text
		tmpMediaType := getMediaInfo(e.MediaObjects)

		fmt.Println(tmpIndex + " " + tmpType + " " + tmpText + " " + tmpMediaType)

	}

}

func getMediaInfo(mediaObjects []models.MediaObject) string {

	var tmpMediaInfoString string = ""

	if mediaObjects != nil {
		tmpMediaInfoString = "(Media > " + mediaObjects[0].Type + " > " + mediaObjects[0].URL + ")"
	}

	return tmpMediaInfoString
}

func printUserTweets(userTweets *twitter.UserTimeline) {
	enc, err := json.MarshalIndent(userTweets, "", "    ")
	if err != nil {
		panic(err)
	}
	fmt.Println(string(enc))
}

func printTweetError(tweetErr *twitter.TweetErrorResponse) {
	enc, err := json.MarshalIndent(tweetErr, "", "    ")
	if err != nil {
		panic(err)
	}
	fmt.Println(string(enc))
}
