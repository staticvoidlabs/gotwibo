package models

import "time"

// Config struct defines the data model to hold the current configuration given in "config.json".
type Config struct {
	Version    string `json:"version"`
	DebugLevel int    `json:"debugLevel"`
	TwitterIDs string `json:"twitterIDs"`
}

type AuthObject struct {
	APIString    string `json:"twitterAPIString"`
	APIKeySecret string `json:"twitterAPIKeySecret"`
	BearerToken  string `json:"twitterBearerToken"`
}

type Tweet struct {
	TweetID      string
	Text         string
	Type         string
	CreatedAt    string
	MediaKey     string
	MediaObjects []MediaObject
}

type MediaObject struct {
	MediaKey string
	URL      string
	Type     string
}

type MediaObjectsRepo struct {
	MediaObjects []MediaObject
}

type TweetsRepo struct {
	Tweets      []Tweet
	RefreshedAt time.Time
}
